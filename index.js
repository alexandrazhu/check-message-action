const main = async () => {

  try {
    const message = process.env.MESSAGE;
    const beginning = process.env.MESSAGE_BEGINNING;
    const regex = new RegExp("^" + beginning);
    console.log('message: '+message);
    if (regex.test(message)) {
      console.log("Correct message format.");
      process.exit(0);
    } else {
      console.log("Incorrect message format.");
      process.exit(1);
    }
  } catch (error) {
    console.error(error);
      process.exit(1);

  }
};

main();
